using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace beart_api.Model
{
    public class Ping
    {
        public String Answer { get; set; }
        public int Length => Answer.Length;
    }

    public class User
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public String id { get; set; }
        public string login { get; set; } = string.Empty;
    }
}
